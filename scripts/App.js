window.addEventListener('load', ()=>{
    const showContainers = document.querySelectorAll('.show_replies')
    showContainers.forEach((btn)=>{
        btn.addEventListener('click', (event)=>{
            let parentContainer = event.target.closest('.comment__container')
            let _id = parentContainer.id
            if(_id){
                let childrenContainer = parentContainer.querySelectorAll(`[dataset=${_id}]`)
                childrenContainer.forEach((child)=>{
                    child.classList.toggle('opened')
                })
            }
        })
    })
})